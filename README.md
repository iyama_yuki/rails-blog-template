# make-it-quick

ToDoリスト

# Create app container image
generate the Rails skelton app

```bash
$ docker-compose run --no-deps app rails new . --force --database=postgresql
```

# Build the image
Builde the image again

```bash
$ docker-compose build
```

# Start the application

```bash
$ docker-compose up -d
```

# Create the db

```bash
$ docker-compose run --rm app rake db:create
```

# Restart the application

```bash
$ docker-compose up -d
```

# Stop the application

```bash
$ docker-compose down
```

# Generate a new Controller

```bash
$ docker-compose run --rm app rails generate controller [Controller (ex. Welcome)] [Action (ex. index])
# only controller
$ docker-compose run --rm app rails generate controller [Controller (ex. Welcome)]
```

# Generate a new Model

```bash
$ docker-compose run --rm app rails generate model [Model (ex. Article)] [Attribute:Type (ex. title:string text:text)]
```

# Generate a new Migration

```bash
$ docker-compose run --rm app rails generate migration [Add(Attribute)To(Table) (ex. AddArticleToComment)] [Attribute:Type (ex. article:references)]
```

# Check routes

```bash
$ docker-compose run --rm app rails routes
```

# Migrate

```bash
$ docker-compose run --rm app rails db:migrate
# target:production (default:develop)
$ docker-compose run --rm app rails db:migrate RAILS_ENV=production
```

# Run dbconsole

```bash
$ docker-compose run --rm app rails dbconsole
```
